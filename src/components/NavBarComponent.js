import React, { useState } from 'react';
import {
    MDBBtn,
    MDBContainer,
    MDBNavbar,
    MDBNavbarBrand,
    MDBNavbarToggler,
    MDBIcon,
    MDBNavbarNav,
    MDBNavbarItem,
    MDBNavbarLink,
    MDBDropdown,
    MDBDropdownToggle,
    MDBDropdownMenu,
    MDBDropdownItem,
    MDBCollapse
} from 'mdb-react-ui-kit';
import { NavLink } from 'react-router-dom';

const NavBarComponent = () => {
    const [showBasic, setShowBasic] = useState(false);

    return (
        <MDBNavbar expand='lg' light bgColor='light'>
            <MDBContainer fluid>
                <MDBNavbarBrand>ADRWARE-PANIER</MDBNavbarBrand>

                <MDBNavbarToggler
                    aria-controls='navbarSupportedContent'
                    aria-expanded='false'
                    aria-label='Toggle navigation'
                    onClick={() => setShowBasic(!showBasic)}
                >
                    <MDBIcon icon='bars' fas />
                </MDBNavbarToggler>

                <MDBCollapse navbar show={showBasic}>
                    <MDBNavbarNav className='mr-auto mb-2 mb-lg-0'>
                        <MDBNavbarItem>
                            <NavLink to={"/home"}>
                                <MDBNavbarLink className={window.location.pathname === "/home" ? "active" : ""}>
                                    Home
                                </MDBNavbarLink>
                            </NavLink>
                        </MDBNavbarItem>
                        <MDBNavbarItem>
                            <NavLink to={"/list"}>
                                <MDBNavbarLink className={window.location.pathname === "/list" ? "active" : ""}>
                                    List
                                </MDBNavbarLink>
                            </NavLink>
                        </MDBNavbarItem>

                        <MDBNavbarItem>
                            <MDBDropdown>
                                <MDBDropdownToggle tag='a' className={window.location.pathname === "/edit" ? "nav-link active" : "nav-link"} role='button'>
                                    Elements
                                </MDBDropdownToggle>
                                <MDBDropdownMenu>
                                    <NavLink to={"/edit"}>
                                        <MDBDropdownItem link>Edit</MDBDropdownItem>
                                    </NavLink>
                                    <MDBDropdownItem link>Another action</MDBDropdownItem>
                                    <MDBDropdownItem link>Something else here</MDBDropdownItem>
                                </MDBDropdownMenu>
                            </MDBDropdown>
                        </MDBNavbarItem>
                    </MDBNavbarNav>

                    <form className='d-flex input-group w-auto'>
                        <input type='search' className='form-control' placeholder='Type query' aria-label='Search' />
                        <MDBBtn color='primary'>Search</MDBBtn>
                    </form>
                </MDBCollapse>
            </MDBContainer>
        </MDBNavbar >
    );
}

export default NavBarComponent;