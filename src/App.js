import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Login from './pages/Login';
import List from './pages/List';
import SignPage from './pages/Sign';
import Edit from './pages/Edit';
import Home from './pages/Home';


const App = () => {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/home" element={<Home />}></Route>
          <Route path="/login" element={<Login />}></Route>
          <Route path="/sign" element={<SignPage />}></Route>
          <Route path="/list" element={<List />}></Route>
          <Route path="/edit" element={<Edit />}></Route>

          <Route path="*" element={<Login />}></Route>
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default App;
