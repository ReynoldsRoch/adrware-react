import React from 'react';
import NavBarComponent from '../components/NavBarComponent';
import FooterComponent from '../components/FooterComponent';

const Home = () => {

    document.title = "HOME";

    return (
        <div className="container">

            <NavBarComponent></NavBarComponent>

            <br />

            <div className="row">

                <div className="col-lg-6">
                    <h1>HOME</h1>
                </div>

                <div className="col-lg-6">
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Harum, vitae ex! Quod, tenetur modi eius ex ab repellendus laboriosam est et porro? A provident quia sunt ad ducimus accusantium voluptas.</p>
                    <br />
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora dolor atque praesentium soluta quaerat? Alias delectus deserunt veniam officiis illum provident libero dolore sit autem aliquam incidunt non voluptatem consectetur pariatur esse, dolorum saepe repellendus iure dolorem quisquam ipsa! Quas, molestiae recusandae minima veniam eum reprehenderit laudantium reiciendis odit. Temporibus cum odio deserunt voluptate magni accusantium sunt quos voluptates ab sequi eos aliquam in animi quaerat atque ipsa, dolorem doloremque aspernatur officia dolor corporis voluptas. Eaque accusantium facilis quae iusto possimus sequi adipisci similique quaerat deleniti, exercitationem dicta necessitatibus hic excepturi vitae voluptates vero quo error minima nemo commodi consequatur.</p>
                </div>

            </div>

            <br />

            <FooterComponent></FooterComponent>
        </div>

    );
};

export default Home;