import React, { useState } from 'react';
import {
    MDBValidation,
    MDBValidationItem,
    MDBInput,
    MDBInputGroup,
    MDBBtn,
    MDBNavbar,
    MDBContainer,
    MDBNavbarBrand
} from 'mdb-react-ui-kit';
import FooterComponent from '../components/FooterComponent';
import axios from 'axios';

const Login = () => {

    document.title = "LOGIN";

    function logAdmin() {
        console.log("log");
        var url = "http://localhost:8080/checkLoginAdmin";
        const email = document.getElementById("idEmail").value;
        const password = document.getElementById("idPassword").value;
        console.log(email + '-' + password);
    }

    return (

        <>

            <div className="container">

                <div className="row">
                    <MDBNavbar light bgColor='light'>
                        <MDBContainer fluid>
                            <MDBNavbarBrand tag="span" className='mb-0 h1'>ADRWARE-PANIER</MDBNavbarBrand>
                        </MDBContainer>
                    </MDBNavbar>
                </div>

                <br />

                <div className="row">
                    <div className="col-lg-6 col-md-6">
                        <img width={'100%'} src='https://png.pngtree.com/background/20230412/original/pngtree-car-showroom-lighting-effect-advertising-background-picture-image_2401286.jpg' className='img-fluid shadow-4' alt='...' />
                    </div>
                    <div className="col-lg-6 col-md-6">
                        <h2>LOGIN</h2>
                        <MDBValidation className='row g-3'>
                            <MDBValidationItem feedback='Please enter your email.' invalid>
                                <MDBInputGroup textBefore='@'>
                                    <MDBInput
                                        type='email'
                                        className='form-control'
                                        defaultValue={'admin@gmail.com'}
                                        id='idEmail'
                                        required
                                        label='Email'
                                    />
                                </MDBInputGroup>
                            </MDBValidationItem>
                            <MDBValidationItem>
                                <MDBInput
                                    type='password'
                                    defaultValue={'12345'}
                                    id='idPassword'
                                    required
                                    label='Password'
                                />
                            </MDBValidationItem>
                            <MDBBtn type='button' onClick={logAdmin}>LOG IN</MDBBtn>
                        </MDBValidation>
                    </div>
                </div>

                <br />

                <FooterComponent></FooterComponent>
            </div>

        </>
    );
}

export default Login;