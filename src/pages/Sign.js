import React from 'react';
import {
    MDBValidation,
    MDBValidationItem,
    MDBInput,
    MDBInputGroup,
    MDBBtn,
    MDBCheckbox,
    MDBNavbar,
    MDBContainer,
    MDBNavbarBrand
} from 'mdb-react-ui-kit';
import FooterComponent from '../components/FooterComponent';
import { useState } from 'react';

const SignPage = () => {

    document.title = "SIGN-IN";

    const [formValue, setFormValue] = useState({
        fname: 'Mark',
        lname: 'Otto',
        email: '',
        city: '',
        state: '',
        zip: '',
    });

    const onChange = (e: any) => {
        setFormValue({ ...formValue, [e.target.name]: e.target.value });
    };
    return (
        <div className="container">

            <div className="row">
                <MDBNavbar light bgColor='light'>
                    <MDBContainer fluid>
                        <MDBNavbarBrand tag="span" className='mb-0 h1'>MILK-PRODUCT</MDBNavbarBrand>
                    </MDBContainer>
                </MDBNavbar>
            </div>

            <br />

            <div className="row">
                <div className="col-lg-6 col-md-6">
                    <img width={'100%'} src='https://media.istockphoto.com/id/1451272680/photo/fresh-dairy-products.webp?b=1&s=170667a&w=0&k=20&c=J_1wD_eY5uWyVUOrJznypSyE4lsqO78sDvNFeb31rEc=' className='img-fluid shadow-4' alt='...' />
                </div>
                <div className="col-lg-6 col-md-6">
                    <h2>SIGN-IN</h2>
                    <MDBValidation className='row g-3'>
                        <MDBValidationItem className='col-md-4'>
                            <MDBInput
                                value={formValue.fname}
                                name='fname'
                                onChange={onChange}
                                id='validationCustom01'
                                required
                                label='First name'
                            />
                        </MDBValidationItem>
                        <MDBValidationItem className='col-md-4'>
                            <MDBInput
                                value={formValue.lname}
                                name='lname'
                                onChange={onChange}
                                id='validationCustom02'
                                required
                                label='Last name'
                            />
                        </MDBValidationItem>
                        <MDBValidationItem feedback='Please choose a username.' invalid className='col-md-4'>
                            <MDBInputGroup textBefore='@'>
                                <input
                                    type='text'
                                    className='form-control'
                                    id='validationCustomUsername'
                                    placeholder='Username'
                                    required
                                />
                            </MDBInputGroup>
                        </MDBValidationItem>
                        <MDBValidationItem className='col-md-6' feedback='Please provide a valid city.' invalid>
                            <MDBInput
                                value={formValue.city}
                                name='city'
                                onChange={onChange}
                                id='validationCustom03'
                                required
                                label='City'
                            />
                        </MDBValidationItem>
                        <MDBValidationItem className='col-md-6' feedback='Please provide a valid zip.' invalid>
                            <MDBInput
                                value={formValue.zip}
                                name='zip'
                                onChange={onChange}
                                id='validationCustom05'
                                required
                                label='Zip'
                            />
                        </MDBValidationItem>
                        <MDBValidationItem className='col-12' feedback='You must agree before submitting.' invalid>
                            <MDBCheckbox label='Agree to terms and conditions' id='invalidCheck' required />
                        </MDBValidationItem>
                        <div className='col-12'>
                            <MDBBtn type='submit'>Submit form</MDBBtn>
                            <MDBBtn type='reset'>Reset form</MDBBtn>
                        </div>
                    </MDBValidation>
                </div>
            </div>

            <br />

            <FooterComponent></FooterComponent>
        </div>
    );
};

export default SignPage;